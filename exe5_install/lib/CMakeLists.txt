# in dir lib/
cmake_minimum_required(VERSION 3.1)

# define the library
add_library(heat heat.c)

# directory where to find headers we depend on
target_include_directories(heat PUBLIC ${PROJECT_SOURCE_DIR}/include)

# this header is necessary to compile a code that use libheat
set_target_properties(heat PROPERTIES PUBLIC_HEADER "${PROJECT_SOURCE_DIR}/include/heat.h")

# where to copy the lib at make install
install(TARGETS heat
        DESTINATION lib
        PUBLIC_HEADER DESTINATION include)
