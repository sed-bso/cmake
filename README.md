# CMake hands-on

[Official webpage](https://sed-bso.gitlabpages.inria.fr/cmake)

This CMake hands-on session is presented by the Inria's SED team
(Service d'Expérimentation et Développement) of Inria Bordeaux
Sud-Ouest.

This training session is intended for developers with little or no
CMake experience.  We will discuss the problems CMake addresses, and
incrementally build around CMake a project representative of
“real-world” software packages. During the exercises, we will tackle
**Unix environments** (Linux and OS X) only and not Windows.

Program summary

* basic cmake usage for an existing project: command line interface ...
* create a simple cmake project: syntax, executable, library, sub-directories, options, install/uninstall target
* advanced usage: compiler/linker options, detect external libraries, package configuration file, ctest, cpack