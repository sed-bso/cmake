# define imported target in cmake file
# to be used by "PROJECT"Config.cmake file at installation
macro(export_imported_target namespace targetname filename)

  if (TARGET ${namespace}::${targetname})
    get_target_property(_INCLUDES  ${namespace}::${targetname} INTERFACE_INCLUDE_DIRECTORIES)
    get_target_property(_LIBDIRS   ${namespace}::${targetname} INTERFACE_LINK_DIRECTORIES)
    get_target_property(_LIBRARIES ${namespace}::${targetname} INTERFACE_LINK_LIBRARIES)
    get_target_property(_CFLAGS    ${namespace}::${targetname} INTERFACE_COMPILE_OPTIONS)
    get_target_property(_LDFLAGS   ${namespace}::${targetname} INTERFACE_LINK_OPTIONS)

    file(WRITE "${CMAKE_CURRENT_BINARY_DIR}/${filename}Targets.cmake"
    "add_library(${namespace}::${targetname} INTERFACE IMPORTED)
set_target_properties(${namespace}::${targetname} PROPERTIES INTERFACE_INCLUDE_DIRECTORIES ${_INCLUDES})
set_target_properties(${namespace}::${targetname} PROPERTIES INTERFACE_LINK_DIRECTORIES    ${_LIBDIRS})
set_target_properties(${namespace}::${targetname} PROPERTIES INTERFACE_LINK_LIBRARIES      ${_LIBRARIES})
set_target_properties(${namespace}::${targetname} PROPERTIES INTERFACE_COMPILE_OPTIONS     ${_CFLAGS})
set_target_properties(${namespace}::${targetname} PROPERTIES INTERFACE_LINK_OPTIONS        ${_LDFLAGS})")
  endif()

endmacro()
