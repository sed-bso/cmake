#!/bin/sh

set -x

CC=gcc
CFLAGS="-Wall -O3"
FILES="heat.c heat_seq.c mat_utils.c -lm"
OUTPUT=heat

$CC $CFLAGS $FILES -o $OUTPUT

exit 0
