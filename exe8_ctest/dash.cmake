# CTest script for Build without MPI

set(CMAKE_HANDS_ON_DIR "$ENV{CMAKE_HANDS_ON}")
if (DEFINED CMAKE_HANDS_ON_DIR)
  set(CTEST_SOURCE_DIRECTORY "${CMAKE_HANDS_ON_DIR}/exe8_ctest")
  set(CTEST_BINARY_DIRECTORY "${CMAKE_HANDS_ON_DIR}/exe8_ctest/build")
else()
  message(FATAL_ERROR "CMAKE_HANDS_ON env. variable is not set, please indicate where are your sources.")
endif()

# should ctest wipe the binary tree before running
set(CTEST_START_WITH_EMPTY_BINARY_DIRECTORY TRUE)

set(CTEST_CMAKE_GENERATOR "Unix Makefiles")

find_program(COVERAGE_COMMAND gcov)
find_program(MEMORYCHECK_COMMAND valgrind)
set(CTEST_COVERAGE_COMMAND "${COVERAGE_COMMAND}")
set(CTEST_MEMORYCHECK_COMMAND "${MEMORYCHECK_COMMAND}")

find_program(CMAKE_C_COMPILER gcc)

set(BUILDNAME "${CMAKE_SYSTEM_NAME}")
# Add i386 or amd64
if(CMAKE_SIZEOF_VOID_P EQUAL 8)
    set(BUILDNAME "${BUILDNAME}-amd64")
else()
    set(BUILDNAME "${BUILDNAME}-i386")
endif()
# Add compiler name
get_filename_component(CMAKE_C_COMPILER_NAME ${CMAKE_C_COMPILER} NAME)
set(BUILDNAME "${BUILDNAME}-${CMAKE_C_COMPILER_NAME}")
# Add the build type, e.g. "Debug, Release..."
if(CMAKE_BUILD_TYPE)
    set(BUILDNAME "${BUILDNAME}-${CMAKE_BUILD_TYPE}")
endif(CMAKE_BUILD_TYPE)
if(HEAT_USE_MPI)
    set(BUILDNAME "${BUILDNAME}-mpi")
endif(HEAT_USE_MPI)
set(CTEST_BUILD_NAME "${BUILDNAME}")

# define ctest steps
ctest_start("Experimental")
ctest_configure(OPTIONS "-DCMAKE_C_FLAGS=-g -O0 -Wall --coverage -DCMAKE_EXE_LINKER_FLAGS=--coverage -DCMAKE_C_COMPILER=gcc")
ctest_build()
ctest_test(INCLUDE "-R usage")
ctest_coverage()
ctest_memcheck()
ctest_submit()