#!/bin/bash

# tested on Ubuntu 20.04 with the following
# sudo apt-get update
# sudo apt-get install -y build-essential libopenmpi-dev cmake cmake-data cmake-curses-gui cmake-qt-gui

ROOTDIR=$PWD

cd $ROOTDIR/exe0_nocmake
./build.sh
./heat 100 100 100 0
rm heat

cd $ROOTDIR/exe1_usecmake
mkdir -p build && cd build && rm CMake* -rf
cmake ..
make VERBOSE=1
./heat_seq 100 100 100 0
cd .. && rm build -rf

cd $ROOTDIR/exe2_buildexe
mkdir -p build && cd build && rm CMake* -rf
cmake ..
make VERBOSE=1
./heat_seq 100 100 100 0
cd .. && rm build -rf

cd $ROOTDIR/exe3_buildlib
mkdir -p build && cd build && rm CMake* -rf
cmake ..
make VERBOSE=1
./heat_seq 100 100 100 0
cd .. && rm build -rf

cd $ROOTDIR/exe4_options
mkdir -p build && cd build && rm CMake* -rf
cmake .. -DHEAT_USE_MPI=ON
make VERBOSE=1
./heat_seq 100 100 100 0
mpiexec -n 4 heat_par 100 100 100 2 2 0
cd .. && rm build -rf

cd $ROOTDIR/exe5_install
mkdir -p build && cd build && rm CMake* -rf
cmake .. -DCMAKE_INSTALL_PREFIX=$PWD/install -DBUILD_SHARED_LIBS=ON
make VERBOSE=1
make install
./install/bin/heat_seq 100 100 100 0
cd .. && rm build -rf

cd $ROOTDIR/exe6_findpackage
mkdir -p build && cd build && rm CMake* -rf
cmake ..
make VERBOSE=1
./heat_seq 100 100 100 0
cd .. && rm build -rf

cd $ROOTDIR/exe7_exportconf
mkdir -p build && cd build && rm CMake* -rf
cmake .. -DCMAKE_INSTALL_PREFIX=$PWD/install -DBUILD_SHARED_LIBS=ON
make VERBOSE=1
make install
./install/bin/heat_seq 100 100 100 0
ls install/lib/cmake/heat/HeatConfig.cmake
cd ../external
mkdir -p build && cd build
cmake .. -DCMAKE_PREFIX_PATH=$ROOTDIR/exe7_exportconf/build/install/lib/cmake
make
cd .. && rm build -rf
cd .. && rm build -rf

cd $ROOTDIR/exe8_ctest
mkdir -p build && cd build && rm CMake* -rf
cmake .. -DHEAT_USE_MPI=ON -DCMAKE_C_FLAGS="-Wall -Wunused-parameter -Wundef -Wno-long-long -Wsign-compare -Wmissing-prototypes -Wstrict-prototypes -Wcomment -pedantic --coverage" -DCMAKE_EXE_LINKER_FLAGS="--coverage"
make VERBOSE=1
ctest -V
ctest -R "par"
ctest -D Experimental
cd .. && rm build -rf

cd $ROOTDIR/exe9_cpack
mkdir -p build && cd build && rm CMake* -rf
cmake .. -DHEAT_USE_MPI=ON -DBUILD_SHARED_LIBS=ON
make VERBOSE=1
ctest -V
make package_source
ls heat-1.0.0-Source.tar.gz
make package
ls heat-1.0.0-Linux.deb
cd .. && rm build -rf
