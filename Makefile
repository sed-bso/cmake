#.SUFFIXE: .tex .pdf
#.tex.pdf:
#	rubber -I ~/inria-doc-style --pdf $*.tex
#TEX_FILES= s*/s*.tex

# Define this as an environment variable pointing to a checkout of
# <https://gforge.inria.fr/projects/inria-doc-style>.
#!!INRIA_DOC_STYLE must be defined in your environment!!
#INRIA_DOC_STYLE ?= $(HOME)/INRIA/TOOLS/inria-doc-style

FILES_TEX	:=	$(wildcard ./*.tex)
FILES_PDF	:=	$(FILES_TEX:%.tex=%.pdf)

%.pdf: %.tex
	rubber -f -I $(INRIA_DOC_STYLE) --pdf $<

cmake_hands-on.html: cmake_hands-on.org
	emacs --batch --no-init-file --load publish.el --funcall org-publish-all

all: check $(FILES_PDF) cmake_hands-on.html

usage:
	@echo "make all | clean | superclean"

check:
	@if [ ! -d "$(INRIA_DOC_STYLE)" ]; then					\
	  echo "error: '$(INRIA_DOC_STYLE)' does not exist" >&2 ;		\
	  echo "Define \$$INRIA_DOC_STYLE pointing to a checkout of" >&2 ;	\
	  echo "<https://gforge.inria.fr/projects/inria-doc-style>." >&2 ;	\
	  false ;								\
	fi

install: cmake.tgz

cmake.tgz: $(FILES_PDF) cmake_hands-on.html
	-rm -rf cmake
	mkdir cmake
	cp -rp exe0_nocmake cmake
	cp -rp exe1_usecmake cmake
	cp -rp exe2_buildexe cmake
	cp -rp exe3_buildlib cmake
	cp -rp exe4_options cmake
	cp -rp exe5_install cmake
	cp -rp exe6_findpackage cmake
	cp -rp exe7_exportconf cmake
	cp -rp exe8_ctest cmake
	cp -rp exe9_cpack cmake
	cp -rp images cmake
	cp *.pdf cmake/
	cp *.org cmake/
	cp *.html cmake/
	tar czf cmake.tgz cmake

clean:
	rm -f cmake*.aux cmake*.log cmake*.nav cmake*.out cmake*.snm cmake*.toc cmake*.vrb cmake*.*~

superclean: clean
	rm -rf cmake*.pdf cmake/ cmake.tgz

FILES_CMAKE	=	$(wildcard ./cmake*.tex)
cmake: $(FILES_CMAKE:%.tex=%.pdf)

.PHONY: all install clean superclean check usage
