cmake_minimum_required (VERSION 3.1)
project (Heat)
enable_language(C)

option(HEAT_USE_MPI "Build MPI executable" OFF)
#set(HEAT_USE_MPI ON CACHE BOOL "Build MPI executable")

set( HEAT_ORDER_LIST "1;2;3")
set( HEAT_ORDER "2" CACHE STRING "Choose the scheme order in ${HEAT_ORDER_LIST}")
set_property(CACHE HEAT_ORDER PROPERTY STRINGS ${HEAT_ORDER_LIST})

# libheat
add_subdirectory(lib)

# heat_seq exe
add_executable(heat_seq heat_seq.c mat_utils.h mat_utils.c)

# dependency to libheat
target_link_libraries(heat_seq heat)

# dependency to libm (math)
find_library(M_LIBRARY m)
mark_as_advanced(M_LIBRARY)
if (M_LIBRARY)
  target_link_libraries(heat_seq ${M_LIBRARY})
endif()

# heat_par exe - depends on MPI
if (HEAT_USE_MPI)

  find_package(MPI REQUIRED)
  add_executable(heat_par heat_par.c mat_utils.h mat_utils.c)
  if (MPI_C_INCLUDE_DIRS)
    target_include_directories(heat_par PRIVATE ${MPI_C_INCLUDE_DIRS})
  endif()
  target_link_libraries(heat_par heat ${M_LIBRARY} ${MPI_C_LIBRARIES})

endif(HEAT_USE_MPI)
