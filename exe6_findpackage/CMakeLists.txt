cmake_minimum_required (VERSION 2.8)
project (Heat)
enable_language(C)

# where to find our custom cmake modules
set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake_modules")

# set the rpath config
set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")

option(HEAT_USE_MPI "Build MPI executable" OFF)

# libheat
add_subdirectory(lib)

# heat_seq exe
add_executable(heat_seq heat_seq.c mat_utils.h mat_utils.c)

# dependency to libheat
target_link_libraries(heat_seq PRIVATE heat)

# dependency to libm
find_package(M REQUIRED)
if (TARGET Math::m)
  target_link_libraries(heat_seq PRIVATE Math::m)
endif()

# install heat_seq
install(TARGETS heat_seq DESTINATION bin)

# heat_par exe - depends on MPI
if (HEAT_USE_MPI)

  find_package(MPI REQUIRED)
  add_executable(heat_par heat_par.c mat_utils.h mat_utils.c)
  target_link_libraries(heat_par PRIVATE heat Math::m MPI::MPI_C)
  install(TARGETS heat_par DESTINATION bin)

endif(HEAT_USE_MPI)

# uninstall target
add_custom_target(uninstall "${CMAKE_COMMAND}" -P "${PROJECT_SOURCE_DIR}/cmake_modules/cmake_uninstall.cmake")

## try to find HWLOC for fun with our hand-written module "FindHWLOC.cmake"

# actually look for HWLOC in the system, notice that a custom paths
# may also be given by defining HWLOC_DIR cmake or env. var.
find_package(HWLOC)

# just show that we can use the information of the find to be able to
# use hwloc in heat_seq
target_link_libraries(heat_seq PRIVATE hwloc::hwloc)
